# Uso de SIG en el mundo

Aquí se mete la consulta SQL que está en los archivos: [data stackexchange](https://data.stackexchange.com/gis/query/new)

![Así están ahora](Rplot.png)


La consulta sólo arroja hasta 50,000 registros.
Hay varias etiquetas que hacen referencia a QGIS (qgis-3, pyqgis, qgis-plugins...) y otras varias que hacen referencia a Arc (arcmap-10.2, arcpy, arcgis-desktop); 


