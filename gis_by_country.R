  ###_______gis_trends_from_data.stackexchange.com________________#######
library(maps)
library(dplyr)
library(countrycode)
library(ggplot2)
library(readr)
library(gsubfn)
library(ggseas)
library(stringr)
library(editheme)
set_base_sty()
gis_location <- read_csv("/home/elio/Downloads/QueryResults(1).csv")

gis_location <- filter(gis_location, Month>"2011-06-01" & Month < "2020-05-01")
gis_location %>% mutate(TagName = str_match(TagName, "arc|qgis") %>% as.character()) %>%
  group_by(TagName, Month) %>% summarise_if(is.numeric, sum) %>%
  ggsdc(., aes(x = Month, y = Questions, colour=TagName),
      method = "stl", start = c(2011, 6), frequency = 12, s.window=12, facet.titles = c("Registro",
            "Tendencia", "Estacional", "Irregular")) +
  scale_color_discrete(name = "SIG") +
  geom_line() + ggtitle("Uso de SIG a nivel mundial", 
                        subtitle="datos de data.stackexchange.com/gis/query") +
  labs(caption = "https://gitlab.com/datamarindo, Casabe Díaz") +
  theme_editor() + 
  theme(panel.grid.major = element_line(linetype = 2, color = "gray40", size = .3), 
        strip.background = element_rect( fill = "deepskyblue4", color = "deepskyblue4"),
        strip.text = element_text(color = "gray80")) +
  ylab("Número de preguntas en gis.stackexchange") +
  xlab("")



### como la locación es una variable mezcla de formatos de locaciones, se necesita parsear con:
### capitales, países, nombre en idioma original, nombres/iniciales de estados de eeuu
  data("world.cities")
  hauptstaedte <- filter(world.cities, capital==1)
  data("countrycode_data")
  pays_native <- data_frame(native=c("Deutschland", "Suomi", "España", "Brasil", "Perú", "Magyarorzág",
          "Sverige", "Österreich", "Polska", "Italia", "Belgique", "México"), english=c("Germany",
          "Finland", "Spain", "Brazil", "Peru", "Hungary", "Sweden", 
          "Austria", "Poland", "Italy", "Belgium", "Mexico"))
  ###______270 registros tienen un nombre nativo de esta lista______##
### preparamos los topónimos para su "grep" y hacemos joins ####
  states     <- paste(state.abb, collapse="|")
  statesname <- paste(state.name, collapse = "|")
  countryman <- paste(hauptstaedte$country.etc, collapse = "|")
  capitals   <- paste(hauptstaedte$name, collapse="|")
  pays       <- paste(pays_native$native, collapse="|")

  gis_location$stateabb  <- strapply(gis_location$Location, states, simplify = T)
  gis_location$statename <- strapply(gis_location$Location, statesname, simplify = T)
  gis_location$pais      <- strapply(gis_location$Location, countryman, simplify = T)
### las columnas necesitan "flattening" para la join
  gis_location$pais      <- vapply(gis_location$pais, paste, collapse=", ", character(1L))
  gis_location$capital   <- strapply(gis_location$Location, capitals, simplify = T)
  gis_location$capital   <- vapply(gis_location$capital, paste, collapse=", ", character(1L))
  gis_location$pays      <- strapply(gis_location$Location, pays, simplify = T)
  gis_location$pays      <- vapply(gis_location$pays, paste, collapse=", ", character(1L))
### unimos lo necesario  
  gis_location <- left_join(gis_location, hauptstaedte, by=c("capital"= "name"))
  gis_location <- left_join(gis_location, countrycode_data, by=c("pais"="country.name.en"))
  gis_location <- left_join(gis_location, pays_native, by=c("pays"= "native"))
### es "NA" no NA y "NULL" no NULL !!!_________________________
  gis_location$pais[gis_location$pais=="NA"] <- gis_location$country.etc[gis_location$pais=="NA"]
  gis_location$pais[gis_location$stateabb!="NULL" | gis_location$statename!="NULL"] <- "USA"
  gis_location$pais[gis_location$pays!="NULL"] <-  gis_location$english
  ### mmm faltan algunos valores para QGIS al principio del periodo, para no tener
### que imputar valores solo conservamos la parte continua de la serie
  gis_location <- filter(gis_location, Month>"2011-06-01" & Month < "2017-08-01")
  pythons   <- filter(gis_location, TagName=="arcpy" | TagName=="pyqgis" | TagName=="python" | TagName=="r")
  databases <- filter(gis_location, TagName=="postgis" | TagName=="postgresql" | 
                        TagName=="javascript" | TagName=="geoserver" | TagName=="geojson")
  arcs      <- filter(gis_location, grepl("arc", TagName, fixed=T)) 
  
# ___________ Nuestros grupos de países _____________________________________####
  us         <- filter(databases, stateabb!="NULL" | statename!="NULL" | pais=="USA" | 
                pais == "United States" | pais =="US") %>%
                group_by(TagName, Month) %>%  summarise_if(is.numeric, sum)

  worldgis   <- arcs %>% group_by(TagName, Month) %>%
                summarise_if(is.numeric, sum)
  ### rest_world pais es un vacío "" 
  rest_world <- filter(databases, pais!="USA" & statename=="NULL" & stateabb=="NULL" & 
                       pais!="United States" & pais!="US" & pais!="" & !is.na(pais)) %>%
                group_by(TagName, Month) %>% summarise_if(is.numeric, sum)
  latam      <- filter(arcs, region=="Central America" | region=="South America" |
                           region=="Caribbean") %>%
                group_by(TagName, Month) %>% summarise_if(is.numeric, sum)
  europa     <- filter(pythons, grepl("Europe", region, fixed=T)) %>%
                group_by(TagName, Month) %>% summarise_if(is.numeric, sum)
  asia       <- filter(gis_location, grepl("sia", region, fixed=T)) %>%
                group_by(TagName, Month) %>% summarise_if(is.numeric, sum)
  africa     <- filter(gis_location, grepl("frica", region, fixed=T)) %>%
                group_by(TagName, Month) %>% summarise_if(is.numeric, sum)
### !is.na(Location) = 10,192 registros
### _______by country______________________________________________________####
  gis_location_country <- gis_location %>% group_by(pais, TagName) %>% 
    summarise_if(is.numeric, sum)

### ____________ahora hacemos los plots____________________________________####  
ggsdc(us, aes(x = Month, y = Questions, colour=TagName),
      method = "stl", start = c(2011, 6), frequency = 12, s.window=12, facet.titles = c("registro",
              "tendencia", "estacional", "irregular")) +
    geom_line() + ggtitle("uso de sig web y database, eeuu", 
               subtitle="datos de data.stackexchange.com") +
    ylab("número de preguntas en gis.stackexchange") +
    xlab("")
  
ggsdc(rest_world, aes(x= Month, y = Questions, colour=TagName),
      method = "decompose", start = c(2010, 7), frequency = 12, facet.titles = c("registro",
                "tendencia", "estacional", "irregular")) +
    geom_line() +
    ylab("número de preguntas en gis.stackexchange") +
    xlab("") + ggtitle("uso de sig web y database, fuera de eeuu",
             subtitle = "datos de data.stackexchange.com \n 
             (sólo datos con locación)")
ggsdc(worldgis, aes(x= Month, y = Questions, colour=TagName),
      method = "decompose", start = c(2010, 7), frequency = 12,facet.titles = c("registro",
                "tendencia", "estacional", "irregular")) +
    geom_line() +
    ylab("número de preguntas en gis.stackexchange") +
    xlab("") + ggtitle("uso de sig web y database, total mundial",
                     subtitle = "datos de data.stackexchange.com \n 
             (incluye datos sin locación)")

ggsdc(latam, aes(x= Month, y = Questions, colour=TagName),
      method = "decompose", start = c(2010, 7), frequency = 12, facet.titles = c("registro",
               "tendencia", "estacional", "irregular")) +
    geom_line() +
    ylab("número de preguntas en gis.stackexchange") +
    xlab("") + ggtitle("uso de sig, latinoamérica",
                     subtitle = "datos de data.stackexchange.com")

ggsdc(europa, aes(x= Month, y = Questions, colour=TagName),
      method = "decompose", start = c(2010, 7), frequency = 12, facet.titles = c("registro",
               "tendencia", "estacional", "irregular")) +
    geom_line() +
    ylab("número de preguntas en gis.stackexchange") +
    xlab("") + ggtitle("uso de sig, europa",
                     subtitle = "datos de data.stackexchange.com")
ggsdc(asia, aes(x= Month, y = Questions, colour=TagName),
      method = "decompose", start = c(2010, 7), frequency = 12, facet.titles = c("registro",
               "tendencia", "estacional", "irregular")) +
    geom_line() +
    ylab("número de preguntas en gis.stackexchange") +
    xlab("") + ggtitle("uso de sig, asia",
                     subtitle = "datos de data.stackexchange.com")

ggsdc(africa, aes(x= Month, y = Questions, colour=TagName),
      method = "decompose", start = c(2010, 7), frequency = 12, facet.titles = c("registro",
               "tendencia", "estacional", "irregular")) +
    geom_line() +
    ylab("número de preguntas en gis.stackexchange") +
    xlab("") + ggtitle("uso de sig, áfrica",
                     subtitle = "datos de data.stackexchange.com")
### _________países latinoamericanos______________###
latam_paises      <- filter(gis_location, region=="Central America" | region=="South America" |
                       region=="Caribbean") %>% group_by(TagName, Month, pais) %>% summarise_if(is.numeric,sum)
worldgis_summ   <- gis_location %>% group_by(pays) %>%
  summarise_if(is.numeric, sum)
ggplot(latam_paises, aes(x=Month, y=Questions, colour=pais)) + geom_line() + facet_wrap(~TagName) +
   ggtitle("uso de sig por país, latinoamérica", subtitle="datos de data.stackexchange.com") +
    ylab("número de preguntas gis.stackexchange") +
    xlab("")

### ahora el mapa _______####

